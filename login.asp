<% title="Đăng nhập" %>
    <%
    ' <!-- kiểm tra xem đăng nhập chưa -->
    if session("id") > 0 then
        Response.Write("<script>window.location = '/index.asp'</script>")
    end if
    %>
    <!-- #include  file ="./includes/header.asp" -->
    <%
    '<!-- kiểm tra request là post hay get -->
    If (Request.ServerVariables("REQUEST_METHOD")= "POST") Then
        username = Request.form("username")
        password = Request.form("password")
        If ( Len(username) = 0 And Len(password) = 0 ) Then
            error = "Tài khoản và mật khẩu không được để trống"
        End If
        sql = "SELECT * FROM tbl_users WHERE username = '" + LCase(username) + "' and password = '" + password + "'"
        res.Open sql, conn
        If not res.eof Then
            Session("id") = res("id")
            Session("username") = res("username")
            Response.Write ("<script>showAutoClose('Thành công', 'Đăng nhập thành công', () => {window.location = '/index.asp'})</script>")
        Else 
            error = "Thông tin đăng nhập không chính xác."
        End If
    End If
    %>

    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-center">
                        <img src="./assets/images/login.png" width="100px" height="100px" />
                    </div>
                </div>
                <div class="card-body">
                    <form method="post">
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" name="username" class="form-control" placeholder="Tài khoản">
                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                            </div>
                            <input type="password" name="password" class="form-control" placeholder="Mật khẩu">
                        </div>
                        <div class="form-group justify-content-center d-flex">
                            <input type="submit" value="Đăng nhập" class="btn btn-success">
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-center links">
                        Chưa có tài khoản? <a href="#">Đăng ký</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%
        If (Len(error) > 0) Then
            Response.Write ("<script>showErrorMsg('"&error&"')</script>")
        End If
    %>