$(document).ready(function () {
    
});

const showErrorMsg = (msg) => {
    Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: msg,
    })
}

const showAutoClose = (title, msg, callback) => {
    let timerInterval
    Swal.fire({
      title: title,
      html: msg,
      timer: 2000,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading()
        const b = Swal.getHtmlContainer().querySelector('b')
        timerInterval = setInterval(() => {
          b.textContent = Swal.getTimerLeft()
        }, 100)
      },
      willClose: () => {
        clearInterval(timerInterval)
      }
    }).then(callback)
}